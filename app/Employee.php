<?php

namespace App;
use App\Department;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $guarded = [];
    public function getActiveAttribute($attribute){
        return [
            0 => 'inactive',
            1 => 'active'
        ] [$attribute];
    }

    public function department(){
        return $this->belongsTo(Department::class);
    }

    
}
