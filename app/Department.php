<?php

namespace App;
use App\Employee;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $guarded = [];
    public function getActiveAttribute($attribute){
        return [
            0 => 'inactive',
            1 => 'active'
        ] [$attribute];
    }

    public function employees(){
        return $this->hasMany(Employee::class);
    }

    public function scopeActive($query){
        return $query->where('active', 1);
    }
}
