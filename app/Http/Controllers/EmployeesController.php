<?php

namespace App\Http\Controllers;
use App\Employee;
use App\Department;
use Illuminate\Http\Request;

class EmployeesController extends Controller
{
    public function index(){
        $employees = Employee::all();
        return view('employees.index', compact('employees'));
    }

    public function show(Employee $employee){
        return view('employees.show', compact('employee'));
    }

    public function create(){
        $departments = Department::active()->get();
        return view('employees.create', compact('departments'));
    }

    public function store(Request $request){
        $data = $request->validate([
            'department_id' => 'required',
            'name' => 'required|min:3',
            'email' => 'required|unique:employees|email',
            'active' => 'required',
        ]);

        Employee::create($data);
        return redirect()->route('employee.index')->with('success', 'New Employee has been added successfully!');
    }

    public function edit(Employee $employee){
        $departments = Department::active()->get();
       return view('employees.edit', compact('employee', 'departments'));
    }

    public function update(Employee $employee){
        $data = request()->validate([
            'department_id' => 'required',
            'name' => 'required|min:3',
            'email' => 'required|email',
            'active' => 'required',
        ]);
        $employee->update($data);
        return redirect('/employees')->with('success', 'Employee updated successfully!');
    }

    public function delete(Employee $employee){
        $employee->delete();
        return redirect()->route('employee.index')->with('danger', $employee->name.' has been deleted on our record!');
    }
}
