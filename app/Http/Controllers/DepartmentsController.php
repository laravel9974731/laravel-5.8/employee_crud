<?php

namespace App\Http\Controllers;
use App\Department;
use Illuminate\Http\Request;

class DepartmentsController extends Controller
{
    public function index(){
        $departments = Department::all();
        return view('departments.index', compact('departments'));
    }

    public function create(){
        return view('departments.create');
    }

    public function store(Request $request){
        $data = $request->validate([
            'name' => 'required|min:3',
            'active' => ''
        ]);
        Department::create($data);
        return redirect()->route('department.index')->with('success', 'Department added successfully!');
    }

    public function enableDisable(Department $department){
        if($department->active == 'active'){
            $department->active = 0;
            $msg = $department->name.' is now disabled!';
        }else{
            $department->active = 1;
            $msg = $department->name.' is now enabled!';
        }
        $department->save();
        return redirect()->route('department.index')->with('info', $msg);
    }
}
