<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function(){
    return redirect('/employees');
});
Route::get('/employees', 'EmployeesController@index')->name('employee.index');
Route::get('/employees/create', 'EmployeesController@create')->name('employee.create');
Route::get('/employees/{employee}', 'EmployeesController@show')->name('employee.show');
Route::post('/employees', 'EmployeesController@store')->name('employee.store');
Route::get('/employees/{employee}/edit', 'EmployeesController@edit')->name('employee.edit');
Route::patch('/employees/{employee}', 'EmployeesController@update')->name('employee.update');
Route::delete('/employees/{employee}/delete', 'EmployeesController@delete')->name('employee.delete');


Route::get('/departments', 'DepartmentsController@index')->name('department.index');
Route::get('/departments/create', 'DepartmentsController@create')->name('department.create');
Route::post('/departments', 'DepartmentsController@store')->name('department.store');
Route::patch('/departments/enable-disable/{department}', 'DepartmentsController@enableDisable')->name('enable-disable');