@extends('layout')
@section('title', 'Add New Department')
@section('content')
<div class="row">
    <div class="col-12">
        <h1>Add New Department</h1>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <form action="/departments" method="POST">
            @csrf
            <div class="form-group pb-1">
                <label for="name">Name</label>
                <input type="text" name="name" placeholder="Name" value="{{old('name')}}" class="form-control">
                <div>
                    {{$errors->first('name')}}
                </div>

                <input type="text" name="active" placeholder="Name" value="1" class="form-control" hidden>
            </div>
            <button type="submit" class="btn btn-sm btn-primary">Add Department</button>
        </form>
    </div>
</div>

@endsection