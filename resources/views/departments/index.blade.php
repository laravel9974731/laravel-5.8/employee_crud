@extends('layout')
@section('title', 'Departments')

@section('content')
<div class="row">
    <div class="col-12">
        <h1 class="text-center">Departments</h1>
        <p><a href="/departments/create" class="btn btn-sm btn-primary">Add Departments</a></p>
    </div>


</div>

<div class="row">
    <div class="col-12">
        <div class="col-12">
            {{-- <table class="table table-striped">
                <tr>
                    <thead>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Status</th>
                        <th>Action</th>
                    </thead>
                </tr>

                <tbody>

                    @foreach($departments as $department)
                    <tr>
                        <td>{{$department->id}}</td>
                        <td><a href="/departments/{{$department->id}}">{{$department->name}}
                            </a></td>
                        <td><span
                                class="badge {{$department->active == 'active' ? 'badge-success' : 'badge-danger'}}">{{$department->active}}</span>
                        </td>
                        <td>

                            <form action="/departments/enable-disable/{{$department->id}}" style="display: inline"
                                method="POST" {{$department->active ==
                                'active' ? 'disabled' : ''}}>
                                @csrf
                                @method('PATCH')

                                <button type="submit" class="btn btn-sm btn-primary" {{$department->active == 'active'
                                    ? 'disabled' : ''}}>Enable</button>
                            </form>
                            <form action="/departments/enable-disable/{{$department->id}}" style="display: inline"
                                method="POST" {{$department->active == 'inactive' ? 'disabled' : ''}}>
                                @csrf
                                @method('PATCH')
                                <button type="submit" class="btn btn-sm btn-danger" {{$department->active == 'inactive'
                                    ? 'disabled' : ''}}>Disable</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>

            </table> --}}
            <table id="myTable" class="display">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($departments as $department)
                    <tr>
                        <td>{{$department->id}}</td>
                        <td><a href="#">{{$department->name}}
                            </a></td>
                        <td><span
                                class="badge {{$department->active == 'active' ? 'badge-success' : 'badge-danger'}}">{{$department->active}}</span>
                        </td>
                        <td>

                            <form action="/departments/enable-disable/{{$department->id}}" style="display: inline"
                                method="POST" {{$department->active ==
                                'active' ? 'disabled' : ''}}>
                                @csrf
                                @method('PATCH')

                                <button type="submit" class="btn btn-sm btn-primary" {{$department->active == 'active'
                                    ? 'disabled' : ''}}>Enable</button>
                            </form>
                            <form action="/departments/enable-disable/{{$department->id}}" style="display: inline"
                                method="POST" {{$department->active == 'inactive' ? 'disabled' : ''}}>
                                @csrf
                                @method('PATCH')
                                <button type="submit" class="btn btn-sm btn-danger" {{$department->active == 'inactive'
                                    ? 'disabled' : ''}}>Disable</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection