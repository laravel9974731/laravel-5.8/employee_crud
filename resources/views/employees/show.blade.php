@extends('layout')
@section('title', 'Details for '.$employee->name)

@section('content')
<div class="row">
    <div class="col-12">
        <h1>Details for {{$employee->name}}</h1>
        <p><a href="/employees/{{$employee->id}}/edit" class="btn btn-sm btn-primary">Edit</a></p>
    </div>
    <div class="row">
        <div class="col-12">
            <ul>
                <li>{{$employee->name}}</li>
                <li>{{$employee->email}}</li>
                <li>{{$employee->active}}</li>
            </ul>
        </div>
    </div>
</div>
@endsection