@extends('layout')
@section('title', 'Employees')

@section('content')
<div class="row">
    <div class="col-12">
        <h1 class="text-center">Employees</h1>
        <p><a href="/employees/create" class="btn btn-sm btn-primary">Add Employee</a></p>
    </div>


</div>

<div class="row">
    <div class="col-12">
        <div class="col-12">
            <table id="myTable" class="display">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Department</th>
                        <th>Email</th>
                        <th>Registered</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($employees as $employee)
                    <tr {{$employee->department->active == 'inactive' ? 'hidden': ''}}>
                        <td>{{$employee->id}}</td>
                        <td><a href="/employees/{{$employee->id}}">
                                <img src="https://ui-avatars.com/api/?color=fff&background=random&size=64&name={{$employee->name}}"
                                    alt="" style="border-radius: 50%; width:40px">
                                {{$employee->name}}
                            </a></td>
                        <td>{{$employee->department->name}}</td>
                        <td>{{$employee->email}}</td>
                        <td>{{$employee->created_at->diffForHumans()}}</td>
                        <td><span
                                class="badge {{$employee->active == 'active' ? 'badge-success' : 'badge-danger'}}">{{$employee->active}}</span>
                        </td>
                        <td>
                            <a href="/employees/{{$employee->id}}/edit" class="btn btn-sm btn-primary"><i
                                    class="bi bi-0-circle"></i>Edit</a>
                            <form action="/employees/{{$employee->id}}/delete" style="display: inline" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection