@extends('layout')
@section('title', $employee->name)
@section('content')
<div class="row">
    <div class="col-12">
        <h1>Edit - {{ $employee->name }}</h1>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <form action="/employees/{{$employee->id}}" method="POST">
            @csrf
            @method('PATCH')
            <div class="form-group pb-1">
                <label for="name">Name</label>
                <input type="text" name="name" placeholder="Name" value="{{old('name') ?? $employee->name}}"
                    class="form-control">
                <div>
                    {{$errors->first('name')}}
                </div>
            </div>

            <div class="form-group pb-1">
                <label for="email">Email</label>
                <input type="text" name="email" placeholder="Email" value="{{old('email') ?? $employee->email}}"
                    class="form-control">
                <div>
                    {{$errors->first('email')}}
                </div>
            </div>

            <div class="form-group">
                <label for="department">Department</label>
                <select name="department_id" id="department" class="form-control">
                    <option value="" disabled>Select Department</option>
                    @foreach ($departments as $department)
                    <option value="{{ $department->id }}" {{ $employee->department_id == $department->id ? 'selected':''
                        }}>{{ $department->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="active">Status</label>
                <select name="active" id="active" class="form-control">
                    <option value="" disabled>Select Employee Status</option>
                    <option value="1" {{ $employee->active == 'active' ? 'selected':'' }}>Active</option>
                    <option value="0" {{ $employee->active == 'inactive' ? 'selected':'' }}>Inactive</option>
                </select>
            </div>

            <button type="submit" class="btn btn-sm btn-primary">Update Employee</button>
        </form>
    </div>
    {{-- <table id="myTable">
        <tr>
            <thead>
                <th>ID</th>
                <th>Name</th>
            </thead>
        </tr>
    </table> --}}
</div>

@endsection