@extends('layout')
@section('title', 'Add New Employee')
@section('content')
<div class="row">
    <div class="col-12">
        <h1>Add New Employee</h1>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <form action="/employees" method="POST">
            @csrf
            <div class="form-group pb-1">
                <label for="name">Name</label>
                <input type="text" name="name" placeholder="Name" value="{{old('name')}}" class="form-control">
                <div>
                    {{$errors->first('name')}}
                </div>
            </div>

            <div class="form-group pb-1">
                <label for="email">Email</label>
                <input type="text" name="email" placeholder="Email" value="{{old('email')}}" class="form-control">
                <div>
                    {{$errors->first('email')}}
                </div>
            </div>

            <div class="form-group">
                <label for="department">Department</label>
                <select name="department_id" id="department" class="form-control">
                    <option value="" disabled>Select Department</option>
                    @foreach ($departments as $department)
                        <option value="{{ $department->id }}">{{ $department->name }}</option>
                    @endforeach
                </select>
            </div>


            <div class="form-group">
                <label for="active">Status</label>
                <select name="active" id="active" class="form-control">
                    <option value="" disabled>Select Employee Status</option>
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>
                </select>
            </div>
            

            <button type="submit" class="btn btn-sm btn-primary">Add Employee</button>
        </form>
    </div>
</div>

@endsection