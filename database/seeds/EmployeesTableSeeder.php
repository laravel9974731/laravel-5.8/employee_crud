<?php

use Illuminate\Database\Seeder;
use App\Employee;
class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $departments = 
        [[
            'name' => 'Henry Abayan', 
            'email' => 'henry@mail.com',
            'department_id' => 3,
            'active' => 1
        ],[
            'name' => 'Dave Besay', 
            'email' => 'dave@mail.com',
            'department_id' => 2,
            'active' => 1
        ],[
            'name' => 'Caleb Sabio', 
            'email' => 'caleb@mail.com',
            'department_id' => 1,
            'active' => 0
        ],[
            'name' => 'Manny Bondoc', 
            'email' => 'manny@mail.com',
            'department_id' => 3,
            'active' => 0
        ],[
            'name' => 'Jean Tataro', 
            'email' => 'jean@mail.com',
            'department_id' => 14,
            'active' => 1
        ],[
            'name' => 'Vince David', 
            'email' => 'vince@mail.com',
            'department_id' => 15,
            'active' => 1
        ],[
            'name' => 'Kevin Hersano', 
            'email' => 'kevin@mail.com',
            'department_id' => 4,
            'active' => 0
        ],[
            'name' => 'Marita Bandola', 
            'email' => 'marita@mail.com',
            'department_id' => 13,
            'active' => 1
        ],[
            'name' => 'Jose Mari Cabrera', 
            'email' => 'jose@mail.com',
            'department_id' => 12,
            'active' => 0
        ],[
            'name' => 'Glenn Natal', 
            'email' => 'glenn@mail.com',
            'department_id' => 12,
            'active' => 0
        ],[
            'name' => 'Jairo Aquino', 
            'email' => 'jai@mail.com',
            'department_id' => 18,
            'active' => 0
        ]];
        for($i = 0; $i < count($departments); $i++){
            Employee::create($departments[$i]);
        }
    }
}
