<?php

use Illuminate\Database\Seeder;
use App\Department;
class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departments = ['Admin & Facilities Mgmt', 'Human Resources', 'Technology Solutions', 'Business Development','Investments', 'Marketing', 'Actuarial', 'Research, Innovation & Dev', 'Strategic Plan', 'Cash Management', 'Marine', 'Motor', 'Bond', 'Accounting', 'Claims', 'Compliance', 'Internal Audit', 'Legal Services'];

        for($i = 0; $i < count($departments); $i++){
            Department::create([
                    'name' => $departments[$i]
                ]);
        }
    }
}
