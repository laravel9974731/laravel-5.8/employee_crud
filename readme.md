# CONFIGURATION

## For Sqlite
- cmd: touch database/database.sqlite

++ .ENV CONFIGURATION

DB_CONNECTION=sqlite

## For Mysql
- Create table of choice(Mine I named it to laravel)

++ .ENV CONFIGURATION

DB_CONNECTION=mysql

DB_HOST=127.0.0.1

DB_PORT=3306

DB_DATABASE=laravel

DB_USERNAME=root

DB_PASSWORD=

## Install Dependecies
cmd: composer install

## Generate Key
cmd: php artisan key:generate

## Migrating Database
cmd: php artisan migrate:refresh --seed

## Running the app
cmd: php artisan serve

Then goto: http://localhost:8000


